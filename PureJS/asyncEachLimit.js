const async = require('async');

const iteratee = (item, callback) => {
    const timeout = parseInt(Math.random() * 1000);
    setTimeout(() => {
        try {
            return callback(null, item * 2);
        } catch (er) {
            return callback(er);
        }
    }, timeout);
};


function callback(err, result) {
    if(err) {
        console.log("err:-", err);
        throw err;
    }
    console.log("result:-", result);
}

var itemsArry = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];

// async.eachLimit(itemsArry, 4, (item) => iteratee(item, callback), callback);

// async.each(itemsArry, (item) => iteratee(item, callback), callback);
function eachLimit(arr, limit, fn, cl) {
    function runTask(item) {
        return (new Promise(resolve => {
            resolve((item, cl)=> fn(item, cl))
        }))
        fn(item, cl);
    }
    const tasks   = arr.map(runTask);
    console.log("tasks:-", tasks);
    const results =  Promise.all(tasks);
    console.log("result:-", results);
    // results.forEach(x => console.log(x));
}
eachLimit(itemsArry, 4, iteratee, callback);
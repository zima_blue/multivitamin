function eachLimit(collection, l, iteratee, finalCallback) {
	// code 
  	const  elements = {...collection};
    const collArray = [];
   	const outPut = [];
    
    
    let index = 0;
    for(const [key, value] of Object.entries(elements)) {
    	const obj = {
      	index: index,
        value: value,
        isDone: false,
        isRunning: false
      };
      index++;
      collArray.push(obj);
    }
    
    const collArrayLength = collArray.length;
    const some = collArray.splice(0, l);
    let counter = 0;
    function limit(coll) {
    	coll.forEach((element) => {
        
        const val = element.value;
        element.isRunning = true;
        iteratee(val, (error, result) => {
            element.isRunning = false;
            element.isDone = true;
            counter++;
            outPut[element.index] = result;
            const newEl = collArray.splice(0,1);

            limit(newEl)
            coll.push(newEl);
            coll.forEach((e) => {
                console.log("running elem:-", e.value);
            })
            if(collArrayLength === counter) {
            finalCallback(null, outPut);
            return;
            }
        });
        
        
      })
    }
    
    limit(some);
  
}

const iterate = (item, final) => {
    const timeout = parseInt(Math.random() * 1000);
		console.log("calling item:-", item);
    setTimeout(() => {
        console.log("done item:-", item);
        final(null, item*2);
    }, timeout);
}

var items = Array(20).fill(1).map((x, y) => x + y);

eachLimit(items, 5, iterate, (err, result) => {
    console.log("All array done", err, result);
});
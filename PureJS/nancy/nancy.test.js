// import test from 'ava';
// import {asyncCounter} from 'async-counter';
// import {Nancy, states} from './nancy';
const test = require("ava");
const asyncCounter = require("asyncCounter");
const {Nancy, states} = require("nancy");

test('empty executor results in a pending promise', t => {
    const p = new Nancy(() => {});
    t.is(p.state, states.pending);
});
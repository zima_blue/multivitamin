function Person(name, age) {
    this.name = name;
    this.age = age;
};

Person.prototype.greeting = function() {
    alert('Hi! I\'m ' + this.name.first + '.');
};

function Teacher(name, age, subject) {
    Person.call(this, name, age);
  
    this.subject = subject;
}

Teacher.prototype = Object.create(Person.prototype);

Object.defineProperty(
    Teacher.prototype,
    'constructor', { 
    value: Teacher, 
    enumerable: false, // so that it does not appear in 'for in' loop
    writable: true }
);

/**
 * prototype inhartance  
 * @param {} name 
 */
function Person(name) {
    this.name = name;
    this.sayName = function() {
        return this.name;
    }
}

function Manager(name, salary) {
    Person.call(this, name);
    this.salary = salary;
    this.getSalary = function() {
        return this.salary;
    }
}

Manager.prototype = new Person();
Manager.prototype.constructor = Manager;


var m1 = new Manager('Murgan', 10);
m1.sayName();
m1.getSalary();
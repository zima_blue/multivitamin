const fs = require('fs');
const path = require('path');
const async = require('async');

const iteratee = (item, callback) => {
    const timeout = parseInt(Math.random() * 1000);
    setTimeout(() => {
        try {
            callback(null, item * 2);
        } catch (er) {
            return callback(er);
        }
    }, timeout);
};
const fileArray = ['package.json','package-clone.json'];
// const arr = [1,2,3,4,5, 6];
const obj = {one: 1, two: 2, three: 3, four: 4, five: 5};
// async.map(arr, iteratee, function(err, result) {
//     console.log(err, result);
// });

// async.eachLimit(arr, 2, iteratee, function(err, result) {
//     console.log(err, result);
// });
function iterateeAgain(itemId, callback) {
    const timeout = parseInt(Math.random() * 1000);
    setTimeout(() => {
      console.log("in with item Id:- ",timeout, itemId);
      callback(null, itemId *2);
    }, timeout);
};

async.eachLimit(fileArray, 2, iterateeAgain, function(err, result) {
      if(err) {
        console.log("err : ",err);
        throw err;
      }
      console.log("result:-", result);
});



// function readfile(fileName, callback) {
//     fs.readFile(
//         path.join(__dirname, fileName),
//         'utf-8', function (err, result) {
//             if(err) {
//                 return callback(err)
//             }
    
//             try {
//                 const object = JSON.parse(result)
//                 callback(null, object.name);
//             } catch (er) {
//                 return callback(er);
//             }
//         })
// }


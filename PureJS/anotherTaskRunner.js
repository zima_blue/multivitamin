let taskRunnerFn = (function (){
   function taskRunner(tasks, callback) {
    Object.keys(tasks).forEach(function(taskId) {
        var task = tasks[taskId];
        
        if(task.isRunning || task.done) {
            return;
        }
        var isDone = !task.done && (!task.dependencies || task.dependencies.length === 0);
        var isAllDone = false;
        if (task.dependencies && task.dependencies.length > 0) {
          var doneTask = task.dependencies.filter(function (dependencyId) {
            var lookupTask = tasks[dependencyId];
            return !!(lookupTask && lookupTask.done);
          });
          if(doneTask.length === task.dependencies.length) {
            isAllDone = true;
          }
        }
        
        
        
        if(isDone || isAllDone) {
            task.isRunning = true;
            task.job(function () {
                task.isRunning = false;
                task.done = true;
                var taskKeys = Object.keys(tasks);
                var doneArray = taskKeys.map(function (key) {
                    return tasks[key].done;
                }).filter(function(bool) {
                    return bool
                })

                console.log("doneArray:-", doneArray);
                console.log("taskKeys:-", taskKeys);
                if(doneArray.length === taskKeys.length) {
                    callback();
                    return;
                }
                taskRunner(tasks, callback);
            });
        }

    })
   };

   return taskRunner;
})();


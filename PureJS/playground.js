

var async = require("async");
var log = console.log;
const collectionArray = [1,2,3,4,5];
const iteratee = (item, callback) => {
    
    const timeout = parseInt(Math.random() *1000);
    setTimeout(() => {
        log("in timout item:-", item);
        return callback(null, item*2);
    }, timeout);
    log("----------------")
}
const collectionObject = {one: 1, two:2, three:3, four:4, five:5};
async.eachLimit(collectionObject, 2, iteratee, (err, result) => {
    console.log("print result :-", err, result);
});


// // https://stackoverflow.com/questions/1408289/how-can-i-do-string-interpolation-in-javascript
// // https://github.com/nodejs/help/issues/2192


// // https://stackoverflow.com/questions/28908180/what-is-a-simple-implementation-of-async-waterfall


// function eachLimit(coll, limit, iteratee, callback) {
//     return eachOfLimit(limit)(coll, withoutIndex(wrapAsync(iteratee)), callback);
// }



// const eachOfLimit = (limit) => {
//     return (obj, iteratee, callback) => {
//         // callback = once(callback);
        
//         var nextElem = iterator(obj);
//         var done = false;
//         var canceled = false;
//         var running = 0;
//         var looping = false;

//         function iterateeCallback(err, value) {
//             if (canceled) return
//             running -= 1;
//             if (err) {
//                 done = true;
//                 callback(err);
//             }
//             else if (err === false) {
//                 done = true;
//                 canceled = true;
//             }
//             else if (value === breakLoop || (done && running <= 0)) {
//                 done = true;
//                 return callback(null);
//             }
//             else if (!looping) {
//                 replenish();
//             }
//         }

//         function replenish () {
//             looping = true;
//             while (running < limit && !done) {
//                 var elem = nextElem();
//                 if (elem === null) {
//                     done = true;
//                     if (running <= 0) {
//                         callback(null);
//                     }
//                     return;
//                 }
//                 running += 1;
//                 iteratee(elem.value, elem.key, onlyOnce(iterateeCallback));
//             }
//             looping = false;
//         }

//         replenish();
//     };
// }

// function eachLimit(collection, limit, iteratee, callback) {

// }
// function eachLimit(collection, limit, iteratee, callback) {
//     async function runner(items, cl) {
//         while(items.length > 0 ){
//             await cl(items.pop());
//         }
//     }
//     Promise.all([...Array(limit)].map(() => runner(collection, callback)));
// }

// async function runner(items, fn, cl) {
//     while (items.length > 0)
//       await fn(items.pop(), cl) ;
//   }
  
//   async function eachLimit(items, limit, fn, cl) {
//     Promise.all([...Array(limit)].map(() => runner(items, fn, cl)));
//   }





// eachLimit(collectionArray, 2, iterate, (err, result) => {
//     console.log("print result :-", err, result);
// })

// async function runner(items, fn) {
//     while (items.length > 0)
//       await fn(items.pop()) ;
//   }
  
//   async function eachLimit(items, fn, limit) {
//     Promise.all([...Array(limit)].map(() => runner(items, fn)));
//   }
  
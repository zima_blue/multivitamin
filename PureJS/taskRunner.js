
function runTask(taskQ, cl) {
    const pendingTask = Object.keys(taskQ);
    let current;

    function finish() {
        console.log("current:-", current);
        const index = pendingTask.indexOf(current);
        if(index > -1) {
            pendingTask.splice(index, 1);
        }
        if(pendingTask.length === 0) {
            cl()
        }
        console.log(pendingTask);
    }

    for(let key in taskQ) {
        let task = taskQ[key];
        if(task.dependencies) {
            console.log(key, "task has dependencies");
        } else {
            console.log(key, "task don't has any dependencies");
            current = key;
            task.job(finish);
            delete tasks[key];
            runTask(task)
        }
    }


}
function taskRunner(t, callback) {
    const taskQueue = {...t};

    runTask(taskQueue, callback);
};

var tasks = {
    'a': {
      job: function (finish) {
        setTimeout(function () {
          console.log('a done');
          finish();
        }, 5000);
      }
    },
    'b': {
      job: function (finish) {
        setTimeout(function () {
          console.log('b done');
          finish();
        }, 2000);
      }
    },
    'c': {
      job: function (finish) {
        setTimeout(function () {
          console.log('c done');
          finish();
        }, 2000);
      },
      dependencies: ['a', 'b']
    },
    'd': {
      job: function (finish) {
        setTimeout(function () {
          console.log('d done');
          finish();
        }, 1000);
      },
      dependencies: []
    },
    'e': {
      job: function (finish) {
        setTimeout(function () {
          console.log('e done');
          finish();
        }, 2000);
      },
      dependencies: ['c', 'b']
    }
  };

  taskRunner(tasks, function() {
      console.log("all task done!");
  });
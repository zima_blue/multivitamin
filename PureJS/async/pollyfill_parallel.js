function eachLimit(collection, limit, iteratee, finalCallback) {
    var promises = [];
    
    // for (let i = 0; i < collection.length; i++) {
    const coll1 = {...collection};

    for(let a in coll1) {
      promises.push(new Promise((resolve, reject) => {
        function bar(nodata, data) { resolve(data);}
        iteratee(coll1[a], bar);
      }));
    }
    
    // Return a Promise.all promise of the array
    Promise.all(promises)
        .then((results) => finalCallback(null, results))
}

const iterate = (item, callback) => {
    const rendomValue = Math.random();
    const timeout = rendomValue * 1000;

    setTimeout(()=> {
        // console.log("set t :-", item);
        callback(null, item * 2);
    }, timeout)
}
const coll = [5,2,3,4,5];

eachLimit(coll, 2, iterate, (err, result) => {
    console.log("Array all done", err, result);
});


const obj = {one: 1, two: 2, three:3, four: 4};

eachLimit(obj, 2, iterate, (err, result) => {
    console.log("Array all done", err, result);
});
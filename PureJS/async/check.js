const log = console.log;

function asyncTimeout(item) {
    const dealy = Math.random() * 1000;
    return (new Promise(resolve => {setTimeout(() => resolve(item), delay)}))
        .then(d => `Waited ${d} seconds`);
}

const coll = [1,2,3,4,5];

function runTask(spec) {
    return asyncTimeout(spec)
}

const tasks   = coll.map(runTask); 
const results = await Promise.all(tasks);
results.forEach(x => log(x));
function fn1(callback) {
    setTimeout(function() {
      console.log('Task One');
      callback(null, 1);
    }, 200);
};

function fn2(callback) {
    setTimeout(function() {
      console.log('Task Two');
      callback(null, 2);
    }, 100);
}

function fn3(callback) {
    setTimeout(function() {
      console.log('Task three');
      callback(null, 3);
    }, 200);
}

function cl(err, results) {
    console.log(results);
}

function parallel(coll, callback) {

    let temp = [];
    var l = coll.length;
    
    function myCl(err, res) {
        console.log("err:-", err, "res:-", res);
        temp.push(res);
        console.log("tep:-", temp);
    }

    Promise.all(coll.map(function (node) {
        return new Promise(function (resolve) {
          resolve(node(myCl));
        })
      })).then(function (results) {
        callback(results)
      }).catch(function (error) {
        console.log("error:-", error);
      })
}

parallel([
    fn1,
    fn2,
    fn3
  ],
  cl);
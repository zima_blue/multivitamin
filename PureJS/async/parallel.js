const async = require('async');

function fn1(callback) {
    setTimeout(function() {
      console.log('Task One');
      callback(null, 1);
    }, 200);
};

function fn2(callback) {
    setTimeout(function() {
      console.log('Task Two');
      callback(null, 2);
    }, 100);
}

function fn3(callback) {
    setTimeout(function() {
      console.log('Task three');
      callback(null, 3);
    }, 200);
}

function cl(err, results) {
    console.log(results);
  }
async.parallel([
    fn1,
    fn2,
    fn3
  ],
  cl);
  
  
  // // an example using an object instead of an array
  // async.parallel({
  //   task1: function(callback) {
  //     setTimeout(function() {
  //       console.log('Task One');
  //       callback(null, 1);
  //     }, 200);
  //   },
  //   task2: function(callback) {
  //     setTimeout(function() {
  //       console.log('Task Two');
  //       callback(null, 2);
  //     }, 100);
  //     }
  // }, function(err, results) {
  //   console.log(results);
  //   // results now equals to: { task1: 1, task2: 2 }
  // });
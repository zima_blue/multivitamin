
function getObjArray(coll) {
    var result = Object.keys(coll).map(key => {
        return {[key] :coll[key]};
    });
    return result;
}

function merge(data) {
    var object = data.reduce((obj, item) => {
        var key = Object.keys(item);
        return {...obj, [key]: item[key]}
     } ,{});
     return object;
}

function foo(d) {
    console.log("d:-", d);
}
function limitCall(coll, limit) {
    var data = {...coll};
    var collection = getObjArray(data);
    while(collection.length) {
        var limitData = collection.splice(0, limit);
        var mergeData = merge(limitData);
        foo(mergeData)
    } 
}

limitCall([2,3,4,1,5,6,7,8,9], 2);
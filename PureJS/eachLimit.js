const cl = (err, result) => {
    if(err) {
        console.log("err:-", err);
        throw err;
    }
    console.log("result:-", result);
}
const iteratee = (item, callback) => {
    const timeout = parseInt(Math.random() * 1000);
    setTimeout(() => {
        try {
            return callback(null, item * 2);
        } catch (er) {
            return callback(er);
        }
    }, timeout);
};

function eachLimit(items, limit, iteratee, cl) {
    return callLimitTimes(limit)(items, iteratee, cl);
};

function iterator(obj) {
    var okeys = obj ? Object.keys(obj) : [];
    var i = -1;
    var len = okeys.length;
    return function next() {
        var key = okeys[++i];
        return i < len ? {value: obj[key], key} : null;
    };
};

function callLimitTimes(limit) {
    return (arr, iteratee, callback) => {
        var running = 0;
        var done = false;
        var obj = {...arr};
        var nextElem = iterator(obj);
        var elem = nextElem();
        var looping = false;
        var canceled = false;
        console.log("print element :-", elem);

        function iterateeCallback(err, value) {
            if (canceled) return
            running -= 1;
            if (err) {
                done = true;
                callback(err);
            }
            else if (err === false) {
                done = true;
                canceled = true;
            }
            // else if (value === breakLoop || (done && running <= 0)) {
            //     done = true;
            //     return callback(null);
            // }
            if (!looping) {
                replenish();
            }
        }
  
        function foo() {
            while (running < limit && !done) {
                var elem = nextElem();
                console.log("print element :-", elem);
                if (!elem) {
                    done = true;
                    if(running <=0) {
                        callback(null);
                        return;
                    }
                }
                running += 1;
                iteratee(elem.value, callback);
            }
        }

        foo();
    } 
}

var items = [1,2,3,4,5, 6];


eachLimit(items, 2, iteratee, callback);


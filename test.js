if(!Function.prototype.bind) (function() {
	var arraySlice = Array.prototype.slice;
	Function.prototype.bind = function(otherThis) {
		if(typeof this !== "function"){
			// throw error;
			throw new TypeError("");
		}

		var baseArgs = arraySlice.call(arguments,1);
		var baseArgsLength = baseArgs.length;

		var fBound = function() {
			baseArgs.length = baseArgsLength;
			baseArgs.push.apply(baseArgs, arguments);
			return 
		}

		return fBound;
	}
})()
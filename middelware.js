var Middleware = function() {
    this.list = [];
    let n = 0;
    const next = () => {
        n++;
        execute.call(this);
    };
    const execute = (cb) => {
        this.list[n].call(this, next);
    }

    this.use = function(fn) {
        this.list.push(fn.bind(this));
    }
    
    this.go = function(cb) {
      this.list.push(cb.bind(this));
      execute();
    }
}

// usage
var middleware = new Middleware();

middleware.use(function(next) {
 var self = this;
 setTimeout(function() {
   console.log("first");
   self.hook1 = true;
   next();
 }, 10);
});

middleware.use(function(next) {
//  var self = this;
 setTimeout(function() {
   console.log("second");
   this.hook2 = true;
   next();
 }.bind(this), 10);
});

middleware.use(function(next) {
//  var self = this;
 setTimeout(() => {
   console.log("third");
   this.hook3 = false;
   next();
 }, 10);
});

var start = new Date();
middleware.go(function() {
 console.log(this.hook1); // true
 console.log(this.hook2); // true
 console.log(this.hook3);
 console.log(new Date() - start); // around 20
});
# Descendent selectors
h1 strong { color: red; }
  1. any strong inside h1 is red other are not affected
  2. decendt selector example

p.intro a { color: yellow; }

# Pseudo Classes and Pseudo Elemnet
  1. a:link, a:visited, a:hover, a:active
  2. :first-letter, :first-line, :before, :first-child, :focus

  p.tip:before {content: "HOT TIP!" }
  input:focus { background-color: #FFFFCC; }

# Child selector
  div > h1

# Adjacent Siblings
  h2 + p
# Attribute Selectors
  img[title]
  input[type="text"]
  a[href^="http://"]

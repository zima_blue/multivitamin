const promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("promise1");
        resolve(10);
    }, 200)
})

const promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("promise2")
        resolve(20)
    }, 500)
})

Promise.all([promise1, promise2])
    .then((vlaues) => {
        console.log("all resolved")
        console.log(vlaues)
    })
    .catch((reason) => {
        console.log("one failed ")
        console.log(reason)
    })
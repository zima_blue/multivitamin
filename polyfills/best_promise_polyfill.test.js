var MyPromise = require("./best_promise_polyfill");

test("executor function is called immediately", () => {
    var string;
    new MyPromise(() => {
        string = "foo";
    });

    expect(string).toBe("foo");
})

test("resolution handler is called when promise is resolved", () => {
    var string = "foo";
    var promise = new MyPromise((resolve) => {
        setTimeout(() => {
            resolve(result);            
        }, 100);
    });

    promise.then((result) => {
        expect(result).toBe(string);
    })
})

test("Promise support may resolution handelrs", () => {
    var string = "foo";
    var promise = new MyPromise((resolve) => {
        setTimeout(() => {
            resolve(result);            
        }, 100);
    });

    promise.then((result) => {
        expect(result).toBe(string);
    })

    promise.then((result) => {
        expect(result).toBe(string);
    })
})

test("resolution handlers can be chanined", () => {
    var string = "foo";
    var promise = new MyPromise((resolve) => {
        setTimeout(() => {
            resolve();            
        }, 100);
    });

    promise.then(() => {
        return new MyPromise((resolve) => {
            setTimeout(() => {
                resolve(result);            
            }, 100);
        })
    }).then((result) => {
       expect(result).toBe(string);
    })
})

test("chaining works with non-promise return value", () => {
    var testString = "foo";
    var promise = new MyPromise((resolve) => {
        setTimeout(() => {
            resolve();            
        }, 100);
    });

    promise.then(() => testString)
        .then((result) => {
            expect(result).toBe(string);
        })
})

test("resolution handler can be attached when promise is resolved", () => {
    var testString = "foo";
    var promise = new MyPromise((resolve) => {
        setTimeout(() => {
            resolve(testString);            
        }, 100);
    });

    promise
        .then(() => {
            setTimeout(() => {
                  promise.then((value) => {
                      expect(value).toBe(testString)
                  })        
            }, 100);
        })
})

test("", (params) => {
    
})
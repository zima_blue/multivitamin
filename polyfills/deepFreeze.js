function deepFreeze(obj) {
    var porp = Object.getOwnPropertyNames(obj);

    for(let key in porp) {
        let value = obj[key];
        obj[key] = (value && typeof value === "object") ?
        deepFreeze(obj[key]) : value;
    }
    return Object.freeze(obj);
}
/**
 * Read about function composition here
 * https://medium.com/javascript-scene/master-the-javascript-interview-what-is-function-composition-20dfb109a1a0
 *
 */

var composition = (...arg) => {

    return (...val) => {
        var result;
        arg.forEach((fun, index) =>{
            if(index === 0) {
                result = fun.apply(null, val);
            } else {
                result = fun.call(null, result);
            }
        });

        return result;
    };

}

var sum = (a, b) => a+b;
var mul2 = (a)=> a*2;
var sq = (a) => a*a;

var comFun = composition(sum, mul2, sq);

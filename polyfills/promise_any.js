Promise.any = async function any(iterable) {
    return Promise.all([...iterable].map(promise => {
        return new Promise((resolve, reject) => {
            Promise.resolve(promise).then(reject, resolve)
        })
    })).then(
        errors=> Promise.resolve(errors),
        value=> Promise.resolve(value)
        )
}
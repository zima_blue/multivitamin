const images = document.querySelectorAll("[data-src]");

function preloadImages(img) {
    const src = img.getAttribute("data-src");
    if(!src) {
        return;
    }
    img.src = src;

}

const imageOptions = {
    threshold: 1,
    rootMargin: "0px, 0px, -500px, 0px"
}
const imgObserver = new IntersectionObserver((entries, imgObserver) => {
    entries.forEach(entry => {
        if(!entry.isIntersecting) {
            return;
        } else {
            preloadImages(entry.target)
            imgObserver.unobserve(entry.target)
        }
    })
}, imageOptions);

images.forEach(image => {
    imgObserver.observe(image)
})
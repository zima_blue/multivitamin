/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind
 *
 */
// Steps

/**
* 1. First check in bind function exist
* 2. Assign Function.prototype.bind to a function say (bindFill), the return type (fBound) of this function is a Function
* 3. Throw error if bindFill don't recive scope as function (this => funciton)
* 4. fBound use apply on pased function
* 5. Also write edge case if bind return's a constrotor funciotn
*
*/


// Yes, it does work with `new funcA.bind(thisArg, args)`
if (!Function.prototype.bind) (function(){
    var ArrayPrototypeSlice = Array.prototype.slice;
    Function.prototype.bind = function(otherThis) {
      if (typeof this !== 'function') {
        // closest thing possible to the ECMAScript 5
        // internal IsCallable function
        throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
      }

      var baseArgs= ArrayPrototypeSlice .call(arguments, 1);
      var baseArgsLength = baseArgs.length;
      var fToBind = this;
      var fNOP    = function() {};
          fBound  = function() {
            baseArgs.length = baseArgsLength; // reset to default base arguments
            baseArgs.push.apply(baseArgs, arguments);
            return fToBind.apply(
                   fNOP.prototype.isPrototypeOf(this) ? this : otherThis, baseArgs
            );
          };

      if (this.prototype) {
        // Function.prototype doesn't have a prototype property
        fNOP.prototype = this.prototype;
      }
      fBound.prototype = new fNOP();

      return fBound;
    };
  })();


  var foo = {
    a: 10,
    getA: function(x, y) {
      return this.a + x + y;
    }
  }

  var bar = {
    a: 12
  }

  var newBind = foo.getA.bind(bar, 10, 20);

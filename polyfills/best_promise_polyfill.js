// https://www.youtube.com/watch?v=C3kUMPtt4hY

class MyPromise {
    constructor(executor) {
        this._resolutionQueue = [];
        this._rejectionQueue = [];
        this._state = "pending";
        this._value;
        this._rejectionReason;
        try {
            executor(this._resolve.bind(this), this._reject.bind(this));
        } catch (e) {
            this._reject(e);
        }
    }

    _runResolutionHandlers() {
        while(this._resolutionQueue.length > 0) {
            var resolution = this._resolutionQueue.shift();
            try {
                var returnValue = resolution.handler(this._value);
            } catch (e) {
                resolution.promise._reject(e);
            }
            
            if(returnValue && returnValue instanceof MyPromise) {
                returnValue.then((v) => {
                    resolution.promise._resolve(v);
                }).catch((e) => {
                    resolution.promise._value(e);
                })
            } else {
                resolution.promise._resolve(returnValue);
            }
        }
    }

    _runRejectionHandlers() {
        while(this._rejectionQueue.length > 0) {
            var rejection = this._rejectionQueue.shift();
            var returnValue = rejection.handler(this._rejectionReason);

            if(returnValue && returnValue instanceof MyPromise) {
                returnValue.then((v) => {
                    rejection.promise._resolve(v);
                });
            } else {
                rejection.promise._resolve(returnValue);
            }
        }
    }

    _resolve(value) {
        if (this._state === "pending") {
            this._value = value;
            this._state = "resolved";
            this._runRejectionHandlers();   
        }
    }

    _reject(reason) {
        if(this._state === "pending") {
            this._rejectionReason = reason;
            this._state = "rejection";

            this._runResolutionHandlers();
        }
    }

    then(resolutionHandler) {
        var newPromise = new MyPromise(() => {});
        this._resolutionQueue.push({
            handler: resolutionHandler,
            promise: newPromise
        });
        if( this._state === "resolved") {
            this._runResolutionHandlers();
        }
        if (this._state === "rejected") {
            newPromise.rejected(this._rejectionReason);
        }
        return newPromise;
    }

    catch(rejectionHandler) {
        var newPromise = new MyPromise(() => {});
        this._rejectionQueue.push({
            handler: rejectionHandler,
            promise: newPromise
        });

        if( this._state === "rejected") {
            this._runResolutionHandlers();
        }

        return newPromise;
    }
}

module.exports = MyPromise;
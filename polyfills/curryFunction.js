/**
 * https://wsvincent.com/javascript-currying/
 *
 * must read about rest ans spread operator
 *
 * https://javascript.info/rest-parameters-spread-operator
 */

/**
 *
 * @param {function} fn
 * @param  {...any} args
 *
 * var curried = curry(volume);
 * at very first time args will be undefined and fn.length will be 3
 * moreFun function will return, moreFun can take 1, 2 or all arguments
 * internally it is call curry function
 *
 * curried(2) => moreFun(2) => curry(volume, ...[undefined], ..[2])
 * on calling moreFun, recursion happens it calls curry function internally
 * curry(fn, ...args); here fn = volume and args = [undefinde, 2] => [2]
 *
 * curried(2)(3) => moreFun(3) => curry(volume, ...[2], ...[3]) => (volme, 2, 3);
 *
 * curried(2)(3)(4) => moreFun(4) => curry(volume, ...[2,3], ...[4]) => (volme, 2, 3, 4);
 * in this stage isAllArguments is true
 * volume(...[2,3,4]);
 *
 */

 var curry = (fn, ...args) =>  {
    var isAllArguments = (fn.length <= args.length);
     if(isAllArguments) {
        return fn(...args);
     } else {
        var moreFun = (...more) => curry(fn, ...args, ...more);
        return moreFun;
    }
 }

/**
 * Shorter version of above function
 */

 var curry = (fn, ...args) =>
 (fn.length <= args.length) ?
   fn(...args) :
   (...more) => curry(fn, ...args, ...more);


 var volume = function (l, w, h) {
     return l * w * h;
 }

 var curried = curry(volume);

 curried(2)(3)(4); // 24;











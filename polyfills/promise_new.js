// https://hackernoon.com/implementing-javascript-promise-in-70-lines-of-code-b3592565af0f

const state = {
    pending: "Pending",
    resolved: "Resolved",
    rejected: "Rejected"
}
// =========Setp 1================
class Promise {
    constructor(executor) {
        const resolve = () => {
            this.state = state.resolved;
        };

        const reject = (err) => {
            this.state = state.rejected;
        }

        this.state = state.pending;
        try {
            executor(resolve, reject);
        } catch(err) {
            reject(err);
        }
    }
}

// =========Setp 2===========implementing Static function =====
class Promise {
    constructor(executor) {
        
        const getCallback = state => value => {
            this.value = value;
            this.state = state;
        };

        const resolve = getCallback(state.resolved);
        const reject = getCallback(state.rejected);
        
        this.state = state.pending;
        try {
            executor(resolve, reject);
        } catch(err) {
            reject(err);
        }
    }

    static resolve(value) {
        return new Promise(resolve => resolve(value));
    }

    static reject(value) {
        return new Promise((_, reject) => reject(value));
    }
}

//=========Setp 3 =========Then and state machine=======

class Promise {
    constructor(executor) {
        
        const getCallback = state => value => {
            this.value = value;
            this.state = state;
        };

        const resolve = getCallback(state.resolved);
        const reject = getCallback(state.rejected);
        
        this.state = state.pending;
        try {
            executor(resolve, reject);
        } catch(err) {
            reject(err);
        }
    }

    static resolve(value) {
        return new Promise(resolve => resolve(value));
    }

    static reject(value) {
        return new Promise((_, reject) => reject(value));
    }
}

//=================step 3===========

class Promise {
    constructor(executor) {
        const stateMachine = {
            [states.resolved]: {
                state: states.resolved,
                // Chain mechanism
                then: onResolved => Promise.resolve(onResolved(this.value))
            },
            [states.rejected]: {
                state: states.rejected,
                // Ignore mechanism
                then: _ => this
            },
            [states.pending]: {
                state: states.pending
            }
        };
        const changeState = state => Object.assign(this, stateMachine[state]);
        const getCallback = state => value => {
            this.value = value;
            changeState(state);
        };

        const resolve = getCallback(states.resolved);
        const reject = getCallback(states.rejected);
        changeState(states.pending);
        try {
            executor(resolve, reject);
        } catch (error) {
            reject(error);
        }
    }

    static resolve(value) {
        return new Promise(resolve => resolve(value));
    }

    static reject(value) {
        return new Promise((_, reject) => reject(value));
    }
}
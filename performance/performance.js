var performanceAPI = window.performance;

var timing = window.performance.timing;

// resource entries

function getResourceEntries() {
	return window.performance.getEntriesByType('resource') || [];
}

// prefetch API (network, tabs, memory)
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Link_prefetching_FAQ

function preFetch(url) {
	const xhrRequest = new XMLHttpRequest();
	xhrRequest.open('GET', resourceURL, true);
	xhrRequest.send();
}
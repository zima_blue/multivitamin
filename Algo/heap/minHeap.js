class MinHeap {
    constructor() {
        this.heap = [null];
    }

    getMin() {
        return this.heap[1];
    }

    insert(node) {
        this.heap.push(node);
        
        if(this.heap.length > 1) {
            let current = this.heap.length -1;
            const prev = Math.floor(current/2);
            while(current > 1 && this.heap[prev] > this.heap[current]) {
                [this.heap[prev], this.heap[current]] = [this.heap[current], this.heap[prev]];
                current = prev;
            }
        }
    }
}
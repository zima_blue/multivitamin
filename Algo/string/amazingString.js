/**
You are given a string S, and you have to find all the amazing substrings of S.

Amazing Substring is one that starts with a vowel (a, e, i, o, u, A, E, I, O, U).


Input
    ABEC

Output
    6

Explanation
	Amazing substrings of given string are :
	1. A
	2. AB
	3. ABE
	4. ABEC
	5. E
	6. EC
	here number of substrings are 6 and 6 % 10003 = 6.
  
*/
function amazingSubString(A) {
    var l = A.length;
    var amaz = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"];
    var out = 0;
    for(var i = 0; i < l; i++) {
        var cur = A[i];
        var index = amaz.indexOf(cur);
        if(index !== -1) {
            out += l - i;
        }
    }
    return out;
}

var log = console.log;

log(amazingSubString("ABEC"));

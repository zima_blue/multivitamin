function compare(A, B) {
		var flag = 0;
		var a = A.split(".");
		var b = B.split(".");
        var al = a.length;
        var bl = b.length;
        var max = a;
        var l = al > bl ? al : bl;
        for(var i = 0; i< l; i++) {
            var verA = parseInt(a[i]) || 0;
            var verB = parseInt(b[i]) || 0;
            if(verA == verB) {
                continue;
            } else if(verA > verB) {
				flag = 1;
                max = a;
                break;
            } else if(verA < verB) {
				flag = -1;
                max = b;
                break
            }
        }
        return flag;
    }

var lengthOfLongestSubstring = function(s) {
  var l = s.length;
  if(l == 0 ) {
    return 0;
  }
  if(l == 1) {
    return 1;
  }

  var lookup = new Map();
  var max = start = 0;

  for(var i =0; i< l; i++) {
    var c = s[i];
    if(lookup.has(c) && lookup.get(c)>= start) {
      start = lookup.get(c) +1;
    }
    lookup.set(c, i);
    max = Math.max(max, i-start +1);
  }
  return max;
};

console.log(longestSubString1("abcakpiaosliema"));

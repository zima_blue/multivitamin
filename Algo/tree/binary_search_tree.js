class Node {
  constructor(data) {
    this.data = data;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  insert(data){
    var newNode = new Node(data);

    if(this.root == null) {
      this.root = newNode;
    } else {
      this.insertNewNode(this.root, newNode);
    }
  }

  getRootNode() {
    return this.root;
  }

  inorder(node) {
    if (node !== null) {
      this.inorder(node.left);
      console.log(node.data);
      this.inorder(node.right);
    }
  }



  insertNewNode(node, newNode) {
    if(newNode.data < node.data) {
      if(node.left === null) {
        node.left = newNode;
      } else {
        this.insertNewNode(node.left, newNode);
      }
    } else {
      if(node.right == null) {
        node.right = newNode;
      } else {
        this.insertNewNode(node.right, newNode);
      }
    }
  }

  search(root, key) {
    if(root === null) {
      return null;
    }
    if(root.data == key) {
      return root;
    } else if(root.data < key) {
      return this.search(root.right, key);
    } else {
      return this.search(root.left, key);
    }

  }

  size(root) {
    if(root === null) {
      return 0;
    }
    var leftSize = this.size(root.left);
    var rightSize = this.size(root.right);

    return leftSize + rightSize + 1;
  }

  height(root) {
    if(root === null) {
      return 0;
    }
    var leftHeight = this.height(root.left);
    var rightHeight = this.height(root.right);

    return 1 + Math.max(leftHeight, rightHeight);
  }
  
}

var BST = new BinarySearchTree();
BST.insert(10);
BST.insert(25);
BST.insert(-5);
BST.insert(7);
BST.insert(22);
BST.insert(17);
BST.insert(13);
BST.insert(5);
BST.insert(9);
BST.insert(27);

var root = BST.getRootNode();

console.log(BST);
BST.inorder(root);

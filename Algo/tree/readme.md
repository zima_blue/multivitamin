A binary tree is a tree in which each node has up to two children . Not all trees are binary trees. For example,
this tree is not a binary tree. You could call it a ternary tree.

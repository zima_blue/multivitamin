/**
 * You are given two non-empty linked lists representing two non-negative integers.
 * The digits are stored in reverse order and each of their nodes
 * contain a single digit. Add the two numbers and return it as a linked list.
 * You may assume the two numbers do not contain any leading zero, except
 * the number 0 itself.
 * 
 * Example:
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 * Explanation: 342 + 465 = 807.
 */

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */


var addTwoNumbers = function(l1, l2) {
    if (l1 == null || (l1.val == 0 && l1.next == null)) return l2;
    if (l2 == null || (l2.val == 0 && l2.next == null)) return l1;
    let carry = 0;
    let dummy = new ListNode(null);
    let prev = dummy;
    while (l1 && l2) {
        let sum = l1.val + l2.val + carry;
        if (sum >= 10) {
            sum = sum % 10;
            carry = 1;
        } else {
            carry = 0;
        }
        prev.next = new ListNode(sum);
        prev = prev.next;
        l1 = l1.next;
        l2 = l2.next;
    }
    while (l1) {
        let sum = (carry + l1.val);
        carry = Math.floor(sum / 10);
        sum = sum % 10;
        prev.next = new ListNode(sum);
        prev = prev.next;
        l1 = l1.next;
    }
    while (l2) {
        let sum = carry + l2.val;
        carry = Math.floor(sum / 10);
        sum = sum % 10;
        prev.next = new ListNode(sum);
        prev = prev.next;
        l2 = l2.next;
    }
    if (carry == 1) {
        prev.next = new ListNode(1);
    }
    return dummy.next;
};

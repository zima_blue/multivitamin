var plusOne = function(A){
        var l = A.length;
        var n = l -1;
        var first = 0;
        var flag = 0;
        var num = "";
        while(n>=0) {
            if(A[n] == 9) {
                A[n] = 0;
                flag = 1;
            } else {
                A[n] = A[n] + 1;
                flag = 0;
                break;
            }
            n--;
        }
        for(var i=0; i<l ; i++) {
            if(A[i] == 0)
                num = num + "0";
            else
                num = num + A[i];
        }
        if(flag) {
            num = parseInt('1' + num);
        }
        // console.log(num);
        return num;
    }

/**
Given a non-negative number represented as an array of digits,

add 1 to the number ( increment the number represented by the digits ).

The digits are stored such that the most significant digit is at the head of the list.

Example:

If the vector has [1, 2, 3]

the returned vector should be [1, 2, 4]

as 123 + 1 = 124.

    NOTE: Certain things are intentionally left unclear in this question which you should practice asking the interviewer.
    For example, for this problem, following are some good questions to ask :

        Q : Can the input have 0’s before the most significant digit. Or in other words, is 0 1 2 3 a valid input?

        A : For the purpose of this question, YES
        Q : Can the output have 0’s before the most significant digit? Or in other words, is 0 1 2 4 a valid output?
        A : For the purpose of this question, NO. Even if the input has zeroes before the most significant digit.

*/

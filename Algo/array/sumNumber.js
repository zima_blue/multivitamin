// [1,2,4,5,9,11] = 13 (2,11)

function findSum(arr, target) {
  var n = arr.length -1;
  var j = n;
  var i = 0;
  var result = [];
  while(i < j ) {
    var sum = arr[i] + arr[j];
    if(sum < target) {
      i++;
    } else if(sum > target) {
      j--;
    } else if(sum === target) {
      result.push([arr[i], arr[j]]);
      i++;
      j--;
    }
  }
  console.log(result);
}

findSum([1,2,4,6,9,11], 13);

function non_repeat_substring(str) {
    let start = 0,
    temp = {},
    maxLenght = "";

    for(let end =0; end< str.length; end++) {
        let current = str[end];
        
        if(current in temp) {
            start = Math.max(start, temp[current] + 1);
        }
        temp[current] = end;
        maxLenght = Math.max(maxLenght, end - start + 1);

    }
    return maxLenght;
};

var result = non_repeat_substring("aacpadq");
console.log("result:-", result);
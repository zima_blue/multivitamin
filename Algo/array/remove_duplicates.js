function remove_duplicates(arr) {
    var first = 0,
    second = 1;
    var l = arr.length;

    while(first < l && second < l) {
        if(arr[first] === arr[second]) {
            delete arr[second];
            second++;
        } else {
            first++;
            second++;
        }
    }
    return arr;
}

var a = [2,3,3,3,4,6,6,9];
var result = remove_duplicates(a);
console.log("result:-", result);
function fruites_into_baskets(fruites) {
    let start = 0,
        maxLength = 0,
        frequency = {};
    
    for(let end = 0; end < fruites.length; end++) {
        const rightFruit = fruites[end];

        if(!(rightFruit in frequency)) {
            frequency[rightFruit] = 0;
        }

        frequency[rightFruit] +=1;
        while (Object.keys(frequency).length > 2) {
            const leftFruit = fruites[start];
            frequency[leftFruit] -= 1;
            if(frequency[leftFruit] === 0) {
                delete frequency[leftFruit];
            }
            start +=1;
        }
        maxLength = Math.max(maxLength, end - start + 1);
    }

    return maxLength;
}


const fruites1 = ['A', 'B', 'C', 'B', 'B', 'C'];
let result = fruites_into_baskets(fruites1);
console.log(result);
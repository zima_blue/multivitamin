DEBOUNCE_TIMERfunction getMajorityElement(arr) {
    var candidate;
    var count = 0;
    for(var i = 0; i< arr.length; i++) {
        if(count == 0) {
            candidate = arr[i];
            count = 1;
            continue;
        } else {
            if(candidate == arr[i]) {
                count++;
            } else {
                count--;
            }
        }
    }

    if(count == 0 ) {
        return;
    }
    count = 0;
    for(var i =0; i <arr.length; i++) {
        if(candidate == arr[i]) {
            count++;
        }
    }
    return (count > parseInt(arr.length/2)? candidate: undefined);
}

var arr = [4,7,4,4,7,4,4,9,4,3];

console.log(getMajorityElement(arr));

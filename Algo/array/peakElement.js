function getPeak(arr) {
    if (!arr || arr.length === 0) {
        return null;
    }
    var n = arr.length;
    var start = 0;
    var end = n -1;

    while (start <= end) {
        // get mid
        var m = parseInt((start + end)/2);
        if((m === 0 || arr[m-1] <= arr[m]) 
            && (m == n-1 || arr[m] >= arr[m + 1])) {
                return arr[m];
            } else if(m > 0 && arr[m-1] > arr[m]) {
                end = m -1;
            } else {
                start = m +1;
            }
    }
}

var d = [2,3,14,41,70,40,30];
console.log(getPeak(d));
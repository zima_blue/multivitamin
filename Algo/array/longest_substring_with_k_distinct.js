function longest_substring_with_k_distinct(str, k) {
    let start = 0,
    maxLength = 0,
    temp = {};

    for(let end =0; end < str.length; end++) {
        const current = str[end];
        if(!(current in temp)) {
            temp[current] = 0;
        }

        temp[current] +=1;

        while(Object.keys(temp).length > k) {
            const first = str[start];
            temp[first] -=1;
            if(temp[first] === 0) {
                delete temp[first];
            }
            start += 1;
        }
        maxLength = Math.max(maxLength, end-start+1);

    }
    return maxLength;
}
let result;
result = longest_substring_with_k_distinct("araaci", 2);
console.log(result);
result = longest_substring_with_k_distinct("araaci", 1);
console.log(result);
result = longest_substring_with_k_distinct("cbbebi", 3);
console.log(result);
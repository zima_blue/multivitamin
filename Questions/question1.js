// #1
var obj = {
    prop: function() {},
    foo: 'bar'
  };

var o = Object.freeze(obj);

// o == obj 
// o === obj
// o.foo = 'test'
// will it also freeze object 
// let a = [0];
// Object.freeze(a);

// #2

# 1.Hooks
https://www.youtube.com/watch?v=DtC1G6MEPVU&list=PLCQ3cvOTrX6C4cxlK8c8gReSOXCQavYSz

# 2.Pure component
Regular Component :- 
    1. A regular compoent des not implement the shouldComponentUpdate method. It always returns true by defalut.

Pure Component
    1. A pure component on the other hand implements shouldComponentUpdate with a shallow props and state comparsion.


# 3.Connect function

# 4. Reconciliation vs Fiber
https://www.vskills.in/certification/tutorial/reconciliation-and-react-fiber/
https://blog.logrocket.com/deep-dive-into-react-fiber-internals/

# 5. Advance react lifecycle methods  
https://www.dotnettricks.com/learn/react/advanced-components-life-cycle-methods

# 6. is setState is async ?
1) setState actions are asynchronous and are batched for performance gains. This is explained in the documentation of setState.

setState() does not immediately mutate this.state but creates a pending state transition. Accessing this.state after calling this method can potentially return the existing value. There is no guarantee of synchronous operation of calls to setState and calls may be batched for performance gains.


2) Why would they make setState async as JS is a single threaded language and this setState is not a WebAPI or server call?

This is because setState alters the state and causes rerendering. This can be an expensive operation and making it synchronous might leave the browser unresponsive.

Thus the setState calls are asynchronous as well as batched for better UI experience and performance.

https://stackoverflow.com/questions/36085726/why-is-setstate-in-reactjs-async-instead-of-sync/36087156#:~:text=1)%20setState%20actions%20are%20asynchronous,does%20not%20immediately%20mutate%20this.&text=Thus%20the%20setState%20calls%20are,better%20UI%20experience%20and%20performance.


function getArticle(a) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(`Resolved article ${a}`);
            // console.log('resolving article', a);
        }, Math.random() * 1000)
    });
}

function renderArticle( a ) {
   console.log('rendered article', a);
}

var ids = [1,2,3,4,5,6];














//
// ids.forEach((element) => {
//     let test = getArticle(element);
//     await test.then((response) => {
//       renderArticle(response);
//     });
// });












async function articleInSyn() {
  for(var i=0; i< ids.length; i++) {
    await getArticle(ids[i]).then((result) => {
        renderArticle(result);
    })
  }
}

articleInSyn();













function fetchArticles(ids) {
	var articles = [];
	ids.forEach((id) => {
  	articles.push(getArticle(id));
  })

 return articles;
}
var articles = fetchArticles(ids);

function printArticles(articles, i){
  if(i < articles.length) {
    articles[i].then((a) => {
    	renderArticle(a);
      i++;

      printArticles(articles, i);
    }, (error)=> console.log(`error ${error}`));
  }
}

printArticles(articles, 0);

/*
Lets say we have a page with n articles. You have access to -
- an array 'articles' consisting of article ids,
- a getArticle method which takes an article id and returns a promise to fetch the article, and
- a render article method which takes the article and renders it
In order to optimize the rendering we want to send out all the articles request in parallel, but to provide a good user experience we want to render the articles in the linear order as present in the 'articles' array.
Also we do not want to wait for all the articles before we begin rendering. As soon as article 1 arrives we can render it. Similarly, as soon as article1 and 2 arrive, we can render article2 and so on.
*/



function getData(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(`Resolved data ${id}`);
            console.log('resolving data:-', id);
        }, Math.random() * 1000)
    });
}

function printData( id ) {
   console.log('rendered data', id);
}

var ids = [1,2,3,4,5,6];

function getDataInSequence(arr) {
  const arrPromises = [];
  for (let i = 0; i < arr.length; i += 1) {
  	arrPromises.push(getData(arr[i]))
  }

  Promise.all(arrPromises).then(
  		resArr => resArr.forEach(res =>printData(res)
    ), err => console.log(err)
    );
}

getDataInSequence(ids);
